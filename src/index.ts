// Initializing Dependencies

import fs from 'fs';

import path from 'path';

// express web framework
import express from 'express';
const app = express();
app.disable('x-powered-by'); // hide server software information from response header - better for security

// POST parser
import bodyParser from 'body-parser';
app.use(bodyParser.urlencoded({extended: true}));

// util file
// import * as util from './util';

// Typescript type configs
//import {  } from './types';

// routings
import Routes from './routes';

// main function
async function main()
{
	// log the date started
	// eslint-disable-next-line no-console
	console.log();
	// eslint-disable-next-line no-console
	console.log(new Date().toDateString() + ' ' + new Date().toTimeString());
	
	// port
	const port = 8080;
	
    // Dynamic Routes
    const auth = Routes.auth();
    app.use("/v1/auth", auth);

	// handling 404
	app.get('*', (req, res) =>
	{
		res.status(404);
		res.sendFile('public/errors/404.json', { root: path.join(__dirname, "..") });
	});
	
	// Starting Server
	app.listen(port, () =>
	{
		// eslint-disable-next-line no-console
		console.log(`The server is ready! Running on http://localhost:${port}`);
	});
}

void main(); // run the main function

/* Initializing dependencies */
import express from 'express';
const route = express.Router();

const authHandler = (): express.Router =>
{
    route.get("/", (req, res) => {
        return res.send("[\"test\"]");
    });
	return route;
};

export default authHandler;

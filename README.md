# Discord Bot Test Framework

Hi there. This is a project to help automate integrated testing of discord bots. Instead of having to test actions yourself, this allows you to write automated tests to do it for you (mostly).

*Note: initially, this project will not have any support for actually 'testing' responses - it will just return the results of the actions it conducts.*

## How it Works

1) Use the auth endpoint to get invite links for test bots to add to your server.
2) Add these bots to a **TEST SERVER**. If you put them in an active server, a) people might be angry at you, and b) it could interfere with test results (other messages would also be recorded as part of the test). By default, the invite links give administrator permission to the bots.
3) Make sure your bot-to-be-tested is running in the server, and make sure that any checks that prevent commands from being run by bots are **disabled**.
4) Send your tests to the testing endpoint.
5) Your tests will be run by the authorized bots, sending messages, reacting, as needed.
6) After a specified time, any events that were recieved in response to these test items will be returned to the endpoint.

## Usage

To download and run locally:
1) `git clone https://gitlab.com/drp19/discordTester.git`
2) `cd discordTester`
3) `npm i`
4) `npm start`

WIP: There will be a publicly accessible endpoint, however this is purely for experimenting with the framework and very small use cases. There will be a maximum amount of test actions per server per day.

## API Definition

#### `GET /v1/auth`

Get the discord invite links to add the testing bots to your test server. You will need to add a minimum number of bots equal to the number of distinct actions you need (i.e. a message needs 2 reactions added to it).

|Part|Location|Field|Schema|Description|
|--|--|--|--|--|
|Request| Query | `bots` | `int` | The number of bot invite links to return
|Response| Body | `max_bots` | `int` | The maximum number of bots currently supported by this instance
|Response| Body | `invite_links` | `array[string]` | A list of discord bot invite links. The number of links will be no greater than `max_bots`, even if the `bots` parameter was larger.